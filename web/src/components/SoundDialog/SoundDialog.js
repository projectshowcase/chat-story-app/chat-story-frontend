import React from "react";
import styled from "styled-components";
import BubbleIntegration from "../BubbleIntegration";

const Audio = styled.audio``;
const IFrame = styled.iframe``;

const SoundDialog = props => {
  const { dialog } = props;

  const renderIntegration = (url, width, height) => (
    <BubbleIntegration dialog={dialog}>
      <IFrame
        src={url}
        width={width}
        height={height}
        scrolling="no"
        frameBorder="0"
        allowtransparency="true"
        allow="encrypted-media"
      />
    </BubbleIntegration>
  );

  const renderAudio = () => {
    return (
      <Audio controls>
        <source src={dialog.payload.url} type="audio/mpeg" />
      </Audio>
    );
  };

  if (dialog.payload.url.includes("soundcloud.com")) {
    return renderIntegration(dialog.payload.url);
  } else if (dialog.payload.url.includes("spotify.com")) {
    return renderIntegration(dialog.payload.url, "300px", "80px");
  } else if (dialog.payload.url.includes("tidal.com")) {
    return renderIntegration(dialog.payload.url, "100%;", "96px");
  } else if (dialog.payload.url.includes("music.apple.com")) {
    return renderIntegration(dialog.payload.url, "100%;", "120px");
  } else {
    return renderAudio();
  }
};

SoundDialog.defaultProps = {
  dialog: {
    payload: {
      url: "https://open.spotify.com/embed/track/5b88tNINg4Q4nrRbrCXUmg",
      integrationLogo:
        "https://www.benlcollins.com/wp-content/uploads/2016/01/spotify.png",
      desc: "Happy",
      integrationName: "Spotify"
    }
  }
};

export default SoundDialog;
