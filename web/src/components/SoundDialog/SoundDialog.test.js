import React from "react";
import ReactDOM from "react-dom";
import SoundDialog from "./";
import { Grommet } from "grommet";
import { ThemeProvider } from "styled-components";

it("SoundDialog renders without crashing", () => {
  const div = document.createElement("div");
  const theme = {
    animationDialog: {
      transformX: "0%"
    },
    bubble: {
      radius: "20px 20px 20px 5px",
      color: "black",
      background: "#D4D8DC"
    }
  };

  ReactDOM.render(
    <Grommet>
      <ThemeProvider theme={theme}>
        <SoundDialog />
      </ThemeProvider>
    </Grommet>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
