import React from "react";
import styled, { css } from "styled-components";
import { style as AnimationDialogStyle } from "../AnimationDialog";
import { Box, Text, Image } from "grommet";

const containerStyle = ({ theme }) => css`
  margin: 0px 2.5px;
  border-radius: ${theme.bubble.radius};
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  ${AnimationDialogStyle}
`;

const BubbleBase = styled(Box)([containerStyle]);

const BubbleIntegration = props => {
  const { dialog } = props;

  return (
    <BubbleBase background="light-3" direction="column">
      <Box
        pad={{
          top: "8px",
          bottom: "5px",
          left: "8px",
          right: "8px"
        }}
        gap="xsmall"
        direction="row"
      >
        <Image
          width="20px"
          height="20px"
          round="full"
          fallback="https://dummyimage.com/25x25/828282/fff.png"
          src={dialog.payload.integrationLogo}
        />
        <Text color="dark-3" size="small">
          {dialog.payload.integrationName}
        </Text>
      </Box>
      <Box>{props.children}</Box>
      <Box
        pad={{
          top: "8px",
          bottom: "5px",
          left: "8px",
          right: "8px"
        }}
        gap="xsmall"
        direction="row"
      >
        <Text color="dark-3" size="small">
          {dialog.payload.desc}
        </Text>
      </Box>
    </BubbleBase>
  );
};

BubbleIntegration.defaultProps = {
  dialog: {
    payload: {
      integrationLogo:
        "https://www.benlcollins.com/wp-content/uploads/2016/01/spotify.png",
      desc: "Happy",
      integrationName: "Spotify"
    }
  }
};

export default BubbleIntegration;
