import React from "react";
import ReactDOM from "react-dom";
import BubbleIntegration from "./";
import { Grommet } from "grommet";
import { ThemeProvider } from "styled-components";

it("BubbleIntegration renders without crashing", () => {
  const div = document.createElement("div");
  const theme = {
    animationDialog: {
      transformX: "0%"
    },
    bubble: {
      radius: "20px 20px 20px 5px",
      color: "black",
      background: "#D4D8DC"
    }
  };

  ReactDOM.render(
    <Grommet>
      <ThemeProvider theme={theme}>
        <BubbleIntegration />
      </ThemeProvider>
    </Grommet>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
